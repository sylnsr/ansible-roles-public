docker-auth0-adldap
===================

Deploys the Auth0 AD LDAP connector as a Docker container.

Requirements
------------

 1. In your Auth0 account dashboard, configure an "Active Directory / LDAP" connection and get the `TICKET URL`
 1. Cert files (`.crt` and `.pem`) that you want to use with your connection.
 1.  Docker and dependent libraries on the remote host for automation
     with Ansible.

Role Variables
--------------
`vars` you might put into your inventory.yml file for a group of hosts:

    auth0:
      # your subdomain for the LDAP server
      sub_domain: 
      # your domain 
      domain: 
      # your TLD
      tld: 
      # primary LDAP server
      ldap_server_a:
      # backup/secondary LDAP server 
      ldap_server_b:
      # initial LDAP user for the Auth0 agent to iterate accounts
      ldap_user:
      # password for the initial LDAP user (above)
      ldap_password:
      # the Auth0 "Ticket URL"
      ticket_url: "https://anva.auth0.com/p/ad/81OVLHzM"
      # leave this blank
      temp_api_key:


Dependencies
------------

None (does not depend on other roles)

Example Playbook
----------------

    - hosts: servers
      become: true
      gather_facts: false
      become_method: sudo
      
      roles:
        - auth0-adldap

License
-------

MIT

Docker Image Project
--------------------
The Docker image used in this project is from:
https://github.com/sylnsr/docker-auth0-ad-ldap-connector  
You can of course use your own image - see defaults/main.yml
